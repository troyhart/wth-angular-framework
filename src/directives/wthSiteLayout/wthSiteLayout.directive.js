(function () {
    'use strict';

    angular.module('wthSiteLayout').directive('wthSiteLayout', ['$location', '$window', '$timeout', '$rootScope', function ($location, $window, $timeout, $rootScope) {
        return {
            transclude: true,
            scope: {
                title: '@',
                subtitle: '@',
                iconFile: '@'
            },
            templateUrl: 'src/directives/wthSiteLayout/wthSiteLayout.template.html',
            controller: [function () {
                var vm = this;
                vm.isMenuVerticle = true;
                vm.routeString = undefined;
                vm.isMenuVisible = true;
                vm.isMenuButtonVisible = true;
                vm.menuButtonClicked = undefined;
            }],
            controllerAs: 'vm',
            bindToController: true,
            link: function ($scope, element, attributes) {
                $scope.$on('wth-menu-orientation-changed-event', function (event, data) {
                    $scope.vm.isMenuVerticle = data.isMenuVerticle;
                });

                $scope.$on('wth-menu-item-selected-event', function (event, data) {
                    $scope.vm.routeString = data.route;
                    $location.path(data.route);
                    checkWidth();
                    broadcastMenuState();
                });

                $($window).on('resize.wthSiteLayout', function () {
                    $scope.$apply(function () {
                        checkWidth();
                        broadcastMenuState();
                    });
                });
                $scope.$on('$destroy', function () {
                    $($window).off('resize.wthSiteLayout');
                });

                $scope.vm.menuButtonClicked = function () {
                    $scope.vm.isMenuVisible = !$scope.vm.isMenuVisible;
                    broadcastMenuState();
                    //$scope.$apply();
                };

                function checkWidth() {
                    var width = Math.max($($window).width(), $window.innerWidth);
                    $scope.vm.isMenuVisible = (width > 768);
                    $scope.vm.isMenuButtonVisible = !$scope.vm.isMenuVisible;
                }

                function broadcastMenuState() {
                    var allowHorizontalToggle = !$scope.vm.isMenuButtonVisible;
                    var isVertical = $scope.vm.isMenuVerticle || !allowHorizontalToggle;
                    $rootScope.$broadcast('wth-menu-show', {show: $scope.vm.isMenuVisible, isVertical: isVertical, allowHorizontalToggle: allowHorizontalToggle});
                }

                $timeout(function () {
                    checkWidth();
                }, 0);
            }
        };
    }]);
}());
