(function () {
    'use strict';

    angular.module('wthMenu').directive('wthMenuGroup', [function () {
        return {
            transclude: true,
            scope: {
                label: '@',
                icon: '@'
            },
            require: '^wthMenu',
            templateUrl: 'src/directives/wthMenu/wthMenuGroup.template.html',
            controllerAs: 'vm',
            bindToController: true,
            controller: function () {
                var vm = this;
                vm.isOpen = false;
                vm.isVertical = undefined;//function defined in link function.
                vm.clicked = undefined;//function defined in link function.
                vm.setSubmenuPosition = undefined;//function defined in link function.
                vm.closeMenu = closeMenu;

                function closeMenu() {
                    vm.isOpen = false;
                }
            },
            link: function ($scope, element, attributes, wthMenuController) {
                $scope.vm.isVertical = function () {
                    return wthMenuController.isVertical || element.parents('wth-subitem-section').length > 0;
                };

                $scope.vm.clicked = function () {
                    $scope.vm.isOpen = !$scope.vm.isOpen;

                    if (element.parents('wth-subitem-section').length === 0) {
                        $scope.vm.setSubmenuPosition();
                    }

                    wthMenuController.setOpenMenuScope($scope);
                };

                $scope.vm.setSubmenuPosition = function () {
                    var pos = element.offset();
                    $('.wth-subitem-section').css({'left': pos.left + 20, 'top': 36});
                };
            }
        };
    }]);
}());

