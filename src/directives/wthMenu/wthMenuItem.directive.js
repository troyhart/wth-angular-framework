(function () {
    'use strict';

    angular.module('wthMenu').directive('wthMenuItem', [function () {
        return {
            scope: {
                label: '@',
                icon: '@',
                route: '@'
            },
            require: '^wthMenu',
            templateUrl: 'src/directives/wthMenu/wthMenuItem.template.html',
            controllerAs: 'vm',
            bindToController: true,
            controller: function () {
                var vm = this;
                vm.isActive = undefined;
                vm.isVertical = undefined;
            },
            link: function ($scope, element, attributes, wthMenuController) {
                $scope.vm.isActive = function () {
                    return element === wthMenuController.activeItemElement;
                };
                $scope.vm.isVertical = function () {
                    return wthMenuController.isVertical || element.parents('wth-subitem-section').length > 0;
                };

                element.on('click', function (event) {
                    event.stopPropagation();
                    event.preventDefault();
                    $scope.$apply(function () {
                        wthMenuController.setActiveItem(element);
                        wthMenuController.setRoute($scope.vm.route);
                    });
                });
            }
        };
    }]);
}());

