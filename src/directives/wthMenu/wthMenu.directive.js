(function () {
    'use strict';

    angular.module('wthMenu').directive('wthMenu', ['$rootScope', '$timeout', function ($rootScope, $timeout) {
        return {
            transclude: true,
            scope: {},
            controller: [function () {
                var vm = this;
                vm.activeItemElement = undefined;
                vm.openMenuScope = undefined;
                vm.showMenu = true;
                vm.isVertical = true;
                vm.allowHorizontalToggle = true;
                vm.setActiveItem = undefined;//function defined in link function of directive
                vm.setRoute = undefined;//function defined in link function of directive
                vm.toggleMenuOrientation = undefined;//function defined in link function of directive
                vm.setOpenMenuScope = setOpenMenuScope;

                function setOpenMenuScope(openMenuScope) {
                    vm.openMenuScope = openMenuScope;
                }
            }],
            controllerAs: 'vm',
            bindToController: true,
            templateUrl: 'src/directives/wthMenu/wthMenu.template.html',
            link: function ($scope, element, attributes) {

                $scope.vm.setActiveItem = function (element) {
                    $scope.vm.activeItemElement = element;
                };

                $scope.vm.setRoute = function (route) {
                    $rootScope.$broadcast('wth-menu-item-selected-event', {route: route});
                };

                $scope.vm.toggleMenuOrientation = function () {

                    // close any open menu
                    if ($scope.vm.openMenuScope) {
                        $scope.vm.openMenuScope.vm.closeMenu();
                    }
                    $scope.vm.isVertical = !$scope.vm.isVertical;

                    $rootScope.$broadcast('wth-menu-orientation-changed-event', {isMenuVerticle: $scope.vm.isVertical});
                };

                $timeout(function () {
                    element.find('.wth-selectable-item:first').trigger('click');
                });

                $scope.$on('wth-menu-show', function (event, data) {
                    $scope.vm.showMenu = data.show;
                    $scope.vm.isVertical = data.isVertical;
                    $scope.vm.allowHorizontalToggle = data.allowHorizontalToggle;
                });

                angular.element(document).bind('click', function (event) {
                    if ($scope.vm.openMenuScope && !$scope.vm.isVertical) {
                        if ($(event.target).parent().hasClass('wth-selectable-item')) {
                            return;
                        }
                        $scope.$apply(function () {
                            $scope.vm.openMenuScope.vm.closeMenu();
                        });
                        event.preventDefault();
                        event.stopPropagation();
                    }
                });
            }
        };
    }]);
}());

