(function () {
    'use strict';

    describe('wthVersion module', function () {
        beforeEach(module('wthVersion'));

        describe('wthVersion directive', function () {
            it('should print current wthVersion', function () {
                module(function ($provide) {
                    $provide.value('version', 'TEST_VER');
                });
                inject(function ($compile, $rootScope) {
                    var element = $compile('<span wth-version></span>')($rootScope);
                    expect(element.text()).toEqual('TEST_VER');
                });
            });
        });
    });

}());
