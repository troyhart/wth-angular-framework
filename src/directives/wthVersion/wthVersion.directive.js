(function () {
    'use strict';

    angular.module('wthVersion')
        .directive('wthVersion', ['version', function (version) {
            return function ($scope, element, attributes) {
                element.text(version);
            };
        }]);
}());
