(function () {
    'use strict';

    describe('wthVersion module', function () {
        beforeEach(module('wthVersion'));

        describe('wthVersion filter', function () {
            beforeEach(module(function ($provide) {
                $provide.value('version', 'TEST_VER');
            }));

            it('should replace VERSION', inject(function (wthVersionFilter) {
                expect(wthVersionFilter('before %VERSION% after')).toEqual('before TEST_VER after');
            }));
        });
    });
}());
