(function () {
    'use strict';

    describe('wthVersion module', function () {
        beforeEach(module('wthVersion'));

        describe('wthVersion service', function () {
            it('should return current version', inject(function (version) {
                expect(version).toEqual('0.0.1');
            }));
        });
    });

}());
