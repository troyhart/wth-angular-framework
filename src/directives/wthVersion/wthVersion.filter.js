(function () {
    'use strict';

    angular.module('wthVersion').filter('wthVersion', ['version', function (version) {
        return function (text) {
            return String(text).replace(/\%VERSION\%/mg, version);
        };
    }]);
}());
