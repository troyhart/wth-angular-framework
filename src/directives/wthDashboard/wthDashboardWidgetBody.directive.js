(function () {
        'use strict';

        angular.module('wthDashboard').directive('wthDashboardWidgetBody', ['$compile', directiveDefinitionObject]);

        function directiveDefinitionObject($compile) {
            return {
                templateUrl: 'src/directives/wthDashboard/wthDashboardWidgetBody.template.html',
                requires: ['wthDashboardWidgetBody', 'wthDashboard'],
                controllerAs: 'wthDashboardWidgetBody',
                controller: controllerFunction,
                link: postLinkFunction
            };

            function controllerFunction() {
                var vm = this;// jshint ignore:line
            }

            function postLinkFunction($scope, iElement, iAttrs, controllers) {
                var wthDashboardWidgetBody = controllers[0];
                var wthDashboard = controllers[1];
                var newElement = angular.element($scope.widget.template);
                iElement.append(newElement);
                $compile(newElement)($scope);

                wthDashboardWidgetBody.close = function (widget) {
                    wthDashboard.widgets.splice(wthDashboard.widgets.indexOf(widget), 1);
                };

                wthDashboardWidgetBody.settings = function (widget) {

                };
            }
        }
    }()
);
