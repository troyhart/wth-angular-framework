(function () {
        'use strict';

        angular.module('wthDashboard').directive('wthDashboard', [directiveDefinitionObject]);

        function directiveDefinitionObject() {
            return {
                scope: {
                    title: '@',
                    widgetDefinitions: '=',
                    gridsterOpts: '='
                },
                templateUrl: 'src/directives/wthDashboard/wthDashboard.template.html',
                controllerAs: 'wthDashboard',
                bindToController: true,
                controller: controllerFunction,
                link: postLinkFunction
            };

            function controllerFunction() {

                var vm = this; // jshint ignore:line
                vm.widgets = [];

                vm.addNewWidget = addNewWidget;

                function addNewWidget(widget) {
                    var newWidget = angular.copy(widget.settings);
                    vm.widgets.push(newWidget);
                }

            }

            function postLinkFunction($scope, iElement, iAttrs, wthDashboard) {

            }
        }
    }()
);
