(function () {
    'use strict';

    angular.module('wthAngularFramework', ['wthDashboard','wthMenu','wthSiteLayout','wthVersion']);
}());
