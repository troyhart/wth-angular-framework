# AngularJS SPA Framework

## gulp build

`gulpfile.js` and its configuration `gupl.config.js` comprise the implementation. To use the build system you must first
install all the dependencies. This task is easily accomplished with the following npm command:

```
npm install
```

This command will install all the node dependencies required for development and publishing. Additionally, it will invoke
`bower install` which will fetch all the application runtime dependencies. Finally, this command will execute `gulp build`
to package up a development build.

The build API is defined in `gulpfile.js`. You can get a listing of all available tasks with the command:

```
gulp help
```

All commands listed are easily executed with:

```
gulp <task name>
```

Executing `gulp` without a task name will execute the default task. This is shorthand for:

```
gulp build build:serve watch
```

This will build all the code and launch it with `browser-sync`, then it will watch for changes in the source and trigger
the appropriate build processing to validate and test the code and then dynamically load changes in the browser.

### API

TODO: fix this API documentation. The API has changed.

#### build

task: `build`
depends: 'fonts', 'css', 'templatecache'

This task triggers all source processing and packages a distribution into
the `./build/` directory.

#### build:clean

task: `build:clean`

This task deletes the `./build/` directory.

#### build:serve

task: `build:serve`

This task launches the (sample) application `./build/index.html` using `browser-sync`.

#### package

task: `package`
depends: 'fonts', 'css', 'templatecache'

This task triggers all source processing and packages a distribution into
the `./dist/` directory. Source processing in this case includes uglification and minification.

#### package:clean

task: `package:clean`

This task deletes the `./dist/` directory.

#### package:serve

task: `package:serve`

This task launches the (sample) application `./dist/index.html` using `browser-sync`.

#### fonts

task: `fonts`
depends: `fonts:clean`

This task copies all external fonts into the project's build to be ready
for distribution and local development/testing.

#### fonts:clean

task: `fonts:clean`

This tasks deletes the copied fonts from the temporary staging directory: `./.tmp/fonts/`.

#### css

task: `css`
depends: `css:clean`

This task compiles the project's less/sass source files into parallel
CSS files in the project's build to be ready for distribution and local
development/testing.

#### css:clean

task: `css:clean`

This tasks deletes the compiled CSS files from the temporary staging directory: `./.tmp/**/*.css`.

#### templatecache

task: `templatecache`
depends: `templatecache:clean`

This task transforms all the project's html template files into a single
templates.js file which executes at runtime to inject all the template
source into the angular $templateCache.

#### templatecache:clean

task: `templatecache:clean`

This tasks deletes the composed angular `$templateCache` modules from the temporary staging directory: `./.tmp/templates.js`.

#### vet

task: `vet`

This task verifies/validates all the javascript source code under the project
root directory, reporting any errors. It runs the code through both `jshint` 
and `jscs`.

#### test

task: `test`
depends: `build`, `vet`

This task runs the project's unit tests with `karma` in single pass mode. Use the `watch` task to start the `karma` 
test runner in watch mode. 

#### watch

task: `watch`

This task starts a number of watchers:

* `karma` test runner in watch mode
* watches all LESS source and invokes the `css` task on any change.
* watches all template HTML source and invokes the `templatecache` task on any change.
* watches all javascript source and invokes the `vet` task on any change. 

#### clean

task: `clean`

This task deletes the temporary build directory (`./.tmp/`), the development build directory (`./build/`) and the 
packaged distribution directory (`./dist/`).