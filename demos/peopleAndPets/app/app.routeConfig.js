/**
 * Created by thart on 8/27/15.
 */

(function () {
    'use strict';

    angular.module('app').config(['$routeProvider', configFunction]);

    function configFunction($routeProvider) {
        var routes = [
            {
                url: '/dashboard',
                config: {template: '<app-dashboard></app-dashboard>'}
            }, {
                url: '/people',
                config: {template: '<app-people></app-people>'}
            }, {
                url: '/pets',
                config: {template: '<app-pets></app-pets>'}
            }];

        routes.forEach(function (route) {
            $routeProvider.when(route.url, route.config);
        });
        $routeProvider.otherwise({redirectTo: '/dashboard'});
    }
}());
