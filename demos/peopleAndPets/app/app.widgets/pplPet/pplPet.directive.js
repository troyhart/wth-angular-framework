(function () {
    'use strict';

    angular.module('app.widgets').directive('pplPet', ['dataService', directiveDefinitionObject]);

    function directiveDefinitionObject(dataService) {
        return {
            templateUrl: 'pplPet.template.html',
            controllerAs: 'pplPet',
            controller: controllerFunction,
            link: postLinkFunction
        };

        /**
         *
         */
        function controllerFunction() {
            var vm = this;//jshint ignore:line

            vm.selectedPet = undefined;
            vm.selectedImage = undefined;
            vm.selectedBackgroundImage = undefined;
        }

        /**
         *
         * @param {Object} $scope
         * @param {Object} iElement
         * @param {Object} iAttrs
         * @param {Object} pplPet
         */
        function postLinkFunction($scope, iElement, iAttrs, pplPet) {
            init(pplPet, $scope.widget.widgetSettings);
        }

        // ---------------------------------------------------------------
        // Helper functions
        // ---------------------------------------------------------------

        function init(controller, settings) {
            dataService.getPet(settings.id).then(success, failure);

            function success(result) {
                controller.selectedPet = result;
                controller.selectedImage = controller.selectedPet.profileImage;
                controller.selectedBackgroundImage = controller.selectedPet.profileBackgroundImage;
            }

            function failure(error) {
                alert(error);
            }
        }
    }
}());
