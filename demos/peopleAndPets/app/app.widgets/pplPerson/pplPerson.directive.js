(function () {
    'use strict';

    angular.module('app.widgets').directive('pplPerson', ['dataService', directiveDefinitionObject]);

    function directiveDefinitionObject(dataService) {
        return {
            templateUrl: 'pplPerson.template.html',
            controllerAs: 'pplPerson',
            controller: controllerFunction,
            link: postLinkFunction
        };

        function controllerFunction() {
            var vm = this;// jshint ignore:line

            vm.selectedPerson = undefined;
            vm.selectedImage = undefined;
            vm.selectedBackgroundImage = undefined;
        }

        function postLinkFunction($scope, iElement, iAttrs, pplPerson) {
            init(pplPerson, $scope.widget.widgetSettings);
        }

        // ---------------------------------------------------------------
        // Helper functions
        // ---------------------------------------------------------------

        function init(controller, settings) {
            dataService.getPerson(settings.id).then(success, failure);

            function success(result) {
                controller.selectedPerson = result;
                controller.selectedImage = controller.selectedPerson.profileImage;
                controller.selectedBackgroundImage = controller.selectedPerson.profileBackgroundImage;
            }

            function failure(error) {
                alert(error);
            }
        }
    }
}());
