(function () {
    'use strict';

    angular.module('app.widgets').directive('pplBelongings', ['dataService', directiveFunction]);

    function directiveFunction(dataService) {
        return {
            templateUrl: 'pplBelongings.template.html',
            controllerAs: 'pplBelongings',
            controller: controllerFunction,
            link: postLinkFunction
        };

        function controllerFunction() {
            var vm = this;// jshint ignore:line
            vm.selectedPerson = undefined;
            vm.selectedPersonBelongings = undefined;
        }

        function postLinkFunction($scope, iElement, iAttrs, pplBelongings) {
            init(pplBelongings, $scope.widget.widgetSettings);
        }

        // ---------------------------------------------------------------
        // Helper functions
        // ---------------------------------------------------------------

        function init(controller, settings) {
            dataService.getPerson(settings.personId).then(successPerson, failure);

            function successPerson(result) {
                controller.selectedPerson = result;
                dataService.getBelongings(settings.personId).then(successBelongings, failure);
            }

            function successBelongings(result) {
                controller.selectedPersonalBelongings = result;
            }

            function failure(error) {
                alert(error);
            }
        }
    }
}());
