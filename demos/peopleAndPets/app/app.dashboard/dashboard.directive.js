(function () {
    'use strict';

    angular.module('app.dashboard').directive('appDashboard', [directiveDefinitionObject]);

    function directiveDefinitionObject() {
        return {
            template: '<wth-dashboard title="{{dashboard.title}}" widget-definitions="dashboard.widgetDefinitions" gridster-opts="dashboard.gridsterOpts"></wth-dashboard>',
            scope: {},
            controllerAs: 'dashboard',
            bindToController: true,
            controller: controllerFunction,
            link: postLinkFunction
        };

        function controllerFunction() {
            var vm = this; // jshint ignore:line
            vm.title = 'Application Dashboard';

            vm.gridsterOpts = {
                columns: 8,
                margins: [10, 20],
                outerMargin: false,
                pushing: true,
                floating: false,
                swaping: true
            };

            vm.widgetDefinitions = [{
                title: 'Person',
                settings: {
                    sizeX: 3,
                    sizeY: 2,
                    minSizeX: 2,
                    minSizeY: 2,
                    template: '<ppl-person></ppl-person>',
                    widgetSettings: {id: 'troy'}
                }

            }, {
                title: 'Pet',
                settings: {
                    sizeX: 3,
                    sizeY: 2,
                    minSizeX: 3,
                    minSizeY: 3,
                    template: '<ppl-pet></ppl-pet>',
                    widgetSettings: {id: 'rosco'}
                }

            }, {
                title: 'Personal Belongings',
                settings: {
                    sizeX: 3,
                    sizeY: 1,
                    template: '<ppl-belongings></ppl-belongings>',
                    widgetSettings: {personId: 'troy'}
                }

            }];
        }

        function postLinkFunction($scope, element, attrs, controller) {
        }

        // ..............................................................
        // Implementations...
        // ..............................................................
    }
}());
