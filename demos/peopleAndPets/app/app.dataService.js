(function () {
        'use strict';

        angular.module('app').factory('dataService', ['$timeout', dataServiceFunction]);

        // ----------------------------------------------------------------------------
        // Mock Data
        // ----------------------------------------------------------------------------

        var people = [{
            id: 'jackey',
            name: 'Jackelin Ingrid Slack',
            profileImage: 'jackey.jpg',
            profileBackgroundImage: 'jackey.background.jpg',
            nickNames: ['Slacker'],
            birthday: new Date(new Date(1243814400000)),
            parents: [],
        }, {
            id: 'troy',
            name: 'William Troy Hart',
            profileImage: 'troy.jpg',
            profileBackgroundImage: 'troy.background.jpg',
            nickNames: ['Troyito'],
            birthday: new Date(1243814400000),
            parents: [],
        }, {
            id: 'lilly',
            name: 'Lilly Bella Slack',
            profileImage: 'lilly.jpg',
            profileBackgroundImage: 'lilly.background.jpg',
            nickNames: ['Skinny Emo Girl'],
            birthday: new Date(1243814400000),
            parents: ['jackey', 'troy'],
        }, {
            id: 'eva',
            name: 'Eva Victoria Slack',
            profileImage: 'eva.jpg',
            profileBackgroundImage: 'eva.background.jpg',
            nickName: ['Eva The Terrible'],
            birthday: new Date(1243814400000),
            parents: ['jackey', 'troy'],
        }, {
            id: 'edi',
            name: 'Edith Jasmin Hennigs-Cornell',
            profileImage: 'edi.jpg',
            profileBackgroundImage: 'edi.background.jpg',
            nickNames: [],
            birthday: new Date(1243814400000),
            parents: [],
        }, {
            id: 'stuart',
            name: 'Stuart Hennigs-Cornell',
            profileImage: 'stuart.jpg',
            profileBackgroundImage: 'stuart.background.jpg',
            nickNames: [],
            birthday: new Date(1243814400000),
            parents: [],
        }, {
            id: 'sequoia',
            name: 'Sequoia Hennigs-Cornell',
            profileImage: 'sequoia.jpg',
            profileBackgroundImage: 'sequoia.background.jpg',
            nickNames: [],
            birthday: new Date(1243814400000),
            parents: ['edi', 'stuart'],
        },];

        var pets = [{
            id: 'niko',
            name: 'Niko',
            profileImage: 'niko.jpg',
            profileBackgroundImage: 'niko.background.jpg',
            birthday: new Date(1320105600000),
            master: 'troy',
            caretakers: ['troy', 'jackey', 'lilly', 'eva'],
            habits: ['Pissing on the laundry', 'Fighting with the neighbor cats', 'Walking the dog'],
            identifiables: ['One eye'],
            redeemables: ['Cool cat']
        }, {
            id: 'ozzy',
            name: 'Ozzy',
            profileImage: 'ozzy.jpg',
            profileBackgroundImage: 'ozzy.background.jpg',
            birthday: new Date(1320105600000),
            master: 'troy',
            caretakers: ['troy', 'jackey', 'lilly', 'eva'],
            habits: ['Pawing at the window', 'Eating all the food', 'Wrestling with the dog'],
            identifiables: ['Great big belly', 'Is a little skittish'],
            redeemables: ['Perfectly behaved']
        }, {
            id: 'wolfie',
            name: 'Wolfie',
            profileImage: 'wolfie.jpg',
            profileBackgroundImage: 'wolfie.background.jpg',
            birthday: new Date(1243814400000),
            master: 'lilly',
            caretakers: ['troy', 'jackey', 'lilly', 'eva'],
            habits: ['Shit\'s everywhere', 'Acts bossy'],
            identifiables: ['Extra skinny and either shaved or matted'],
            redeemables: ['She just wants some love']
        }, {
            id: 'luna',
            name: 'Luna',
            profileImage: 'luna.jpg',
            profileBackgroundImage: 'luna.background.jpg',
            birthday: new Date(1243814400000),
            master: 'eva',
            caretakers: ['troy', 'jackey', 'lilly', 'eva'],
            habits: ['Puke\'s everywhere', 'Loves to put her but in your face', 'Stays inside to avoid other animals who frighten her'],
            identifiables: ['Super cute white paws against a beautiful black coat'],
            redeemables: ['She just wants some love']
        }, {
            id: 'rosco',
            name: 'Rosco',
            profileImage: 'rosco.jpg',
            profileBackgroundImage: 'rosco.background.jpg',
            birthday: new Date(1403308800000),
            master: 'jackey',
            caretakers: ['troy', 'jackey', 'lilly', 'eva'],
            habits: ['Steals hearts', 'Wrestles you when you try to get out of bed', 'Avoids rattlesnakes', 'Decides when he wants to listen', 'Loves the almond roca'],
            identifiables: ['Extreme cuteness', 'Brown lips', 'Shaggy tan coat'],
            redeemables: ['Almost everything he does']
        }];

        var cars = [{
            id: 'car.sprinter',
            acquisitionDate: new Date(1403308800000),
            owner: 'troy',
            name: 'white monster',
            description: 'sprinter van.'
        }];

        var motorcycles = [{
            id: 'moto.bmw',
            acquisitionDate: new Date(1403308800000),
            owner: 'troy',
            name: 'bmer',
            description: 'bmw gs 1150 adventurer.'
        }];

        var bicycles = [{
            id: 'bk.commuter1',
            acquisitionDate: new Date(1403308800000),
            owner: 'troy',
            name: 'commuter',
            description: 'green/blue with fenders and a rack.'
        }];

        function dataServiceFunction($timeout) {

            return {
                getPeople: getPeople,
                getPerson: getPerson,
                getPets: getPets,
                getPet: getPet,
                getBelongings: getBelongings,
            };

            // ----------------------------------------------------------------------------
            // Service methods
            // ----------------------------------------------------------------------------
            function getPeople() {
                return $timeout(function () {
                    return people;
                }, 500);
            }

            function getPerson(id) {
                return $timeout(function () {
                    for (var i = 0; i < people.length; i++) {
                        if (people[i].id === id) {
                            return people[i];
                        }
                    }
                    return undefined;
                }, 300);
            }

            function getPets() {
                return $timeout(function () {
                    return pets;
                }, 500);
            }

            function getPet(id) {
                return $timeout(function () {
                    for (var i = 0; i < pets.length; i++) {
                        if (pets[i].id === id) {
                            return pets[i];
                        }
                    }
                    return undefined;
                }, 300);
            }

            function getBelongings(personId) {
                return $timeout(function () {
                    var belongings = {};
                    // Add other collections of items. Each item in each
                    // collection must have an 'owner' property to match
                    // the identity of a person.
                    [
                        {
                            name: 'bicycles',
                            itemCollection: bicycles
                        },
                        {
                            name: 'cars',
                            itemCollection: cars
                        },
                        {
                            name: 'motorcycles',
                            itemCollection: motorcycles
                        }
                    ].forEach(function (namedItemCollection) {
                            var ownedItems = (belongings[namedItemCollection.name] = []);
                            namedItemCollection.itemCollection.forEach(function (item) {
                                if (personId === item.owner) {
                                    ownedItems.push(item);
                                }
                            });
                        });
                    return belongings;
                }, 500);
            }

            // ----------------------------------------------------------------------------
            // Helper methods
            // ----------------------------------------------------------------------------
        }
    }()
);
