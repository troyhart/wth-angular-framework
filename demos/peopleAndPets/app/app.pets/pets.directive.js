(function () {
    'use strict';

    angular.module('app-pets').directive('appPets', [directiveDefinitionObject]);
    function directiveDefinitionObject() {
        return {
            restrict: 'E',
            templateUrl: 'pets.html',
            controllerAs: 'vm',
            controller: controllerFunction,
            link: postLinkFunction
        };

        function controllerFunction() {
            var vm = this; // jshint ignore:line
        }

        function postLinkFunction($scope, elements, attributes, appPets) {

        }
    }
}());
