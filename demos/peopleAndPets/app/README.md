# Sample application #

This is a simple application demonstrating usage of the `wth*` `components`. `wth` is nothing more than my initials; I needed a prefix for my component names and I couldn't muster any creativity.

At some point this project will re-organize to separate the `components` from the sample application. For now, just understand that the `components` directory is where the meaningful bits are located.