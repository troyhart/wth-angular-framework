(function () {
    'use strict';

    // Declare app level module which depends on views, and directives
    angular.module('app', [
        'ngRoute',
        'ngAnimate',
        'wthVersion',
        'wthSiteLayout',
        'wthMenu',
        'wthDashboard',
        'app.widgets',
        'app.dashboard',
        'app.people',
        'app.pets'
    ]);
}());
