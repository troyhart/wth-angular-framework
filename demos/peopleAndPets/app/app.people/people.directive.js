(function () {
    'use strict';

    angular.module('app.people').directive('appPeople', [directiveDefinitionObject]);

    function directiveDefinitionObject() {
        return {
            restrict: 'E',
            templateUrl: 'people.html',
            controllerAs: 'vm',
            controller: controllerFunction,
            link: postLinkFunction
        };

        function controllerFunction() {

        }

        function postLinkFunction($scope, elements, attributes, appPeople) {

        }
    }
}());
