'use strict';
// jshint node:true

/**
 * yargs variables can be passed in to alter the behavior, when present.
 * Example: gulp build --verbose
 *
 * Recognized arguments:
 *
 * --verbose  : will increase the verbosity of logs.
 */

var pkg = require('../../package.json');

var gulp = require('gulp');
var $ = require('gulp-load-plugins')({lazy: true});

var browserSync = require('browser-sync').create();
var args = require('yargs').argv;

var config = require('../config');
var log = require('../util/log');

/**
 * A task that packages the build directory into the dist directory. This task handles all the uglification,
 * minification, source map generation, and so forth. This package does not trigger build, it expects to package
 * a build that has been tested.
 */
gulp.task('package', ['package:js', 'package:css', 'package:fonts', 'package:exdep'], function () {
    log({package: 'Injecting sample app: `' + config.sampleIndex + '` --> `' + config.distDir + config.sampleIndex.substr(2) /*strip `./` from sampleIndex*/ + '`'});

    // TODO: inject dependencies into index.html and then add a 'package:serve' task.
});

gulp.task('package:js', function () {
    log({'package:js': 'Minifying and concating local javascript: `' + config.buildDir + 'directives/**/*.js (!*_test.js)` --> `' + config.distDir + pkg.name + '.js`'});

    var buildSrc = [
        '!' + config.buildDir + '{directives,services}/**/*_test.js',
        config.buildDir + pkg.name + '.module.js',
        config.buildDir + 'directives/**/*.module.js',
        config.buildDir + '{directives,services}/**/*.js'];

    return gulp.src(buildSrc)
        .pipe($.plumber())
        .pipe($.if(args.verbose, $.debug({title: 'package:js'})))
        .pipe($.sourcemaps.init())
        .pipe($.concat(pkg.name + '.js'))
        .pipe(gulp.dest(config.distDir))
        .pipe($.rename(pkg.name + '.min.js'))
        .pipe($.uglify())
        .pipe($.sourcemaps.write('./'))
        .pipe(gulp.dest(config.distDir));
});

gulp.task('package:css', function () {
    log({'package:css': 'Minifying and concating local CSS: `' + config.buildDir + 'directives/**/*.css` --> `' + config.distDir + pkg.name + '.css`'});

    return gulp.src([config.buildDir + 'directives/**/*.css'])
        .pipe($.plumber())
        .pipe($.if(args.verbose, $.debug({title: 'package:css'})))
        .pipe($.sourcemaps.init())
        .pipe($.concat(pkg.name + '.css'))
        .pipe(gulp.dest(config.distDir))
        .pipe($.rename(pkg.name + '.min.css'))
        .pipe($.minifyCss())
        .pipe($.sourcemaps.write('./'))
        .pipe(gulp.dest(config.distDir));
});

gulp.task('package:fonts', function () {
    log({'package:fonts': 'Copying fonts:  `' + config.buildDir + 'fonts/**` --> `' + config.distDir + 'fonts/**`'});

    return gulp.src([config.buildDir + 'fonts/**'])
        .pipe($.plumber())
        .pipe($.if(args.verbose, $.debug({title: 'package:fonts'})))
        .pipe(gulp.dest(config.distDir + 'fonts/'))
        .pipe(browserSync.reload({stream: true}));
});

/**
 * A task to copy all the installed bower dependencies into the build directory.
 */
gulp.task('package:exdep', function () {
    log({'package:exdep': 'Copying bower dependencies: `' + config.bowerDir + '**` --> `' + config.distDir + 'bower_components/**`'});

    return $.bower()
        .pipe(gulp.dest(config.distDir + 'bower_components/'))
        .pipe(browserSync.reload({stream: true}));
});