'use strict';
// jshint node:true

/**
 * yargs variables can be passed in to alter the behavior, when present.
 * Example: gulp build --verbose
 *
 * Recognized arguments:
 *
 * --verbose  : will increase the verbosity of logs.
 */

var pkg = require('../../package.json');

var gulp = require('gulp');
var $ = require('gulp-load-plugins')({lazy: true});

var browserSync = require('browser-sync').create();
var wiredep = require('wiredep').stream;
var args = require('yargs').argv;

var config = require('../config');
var log = require('../util/log');

var localDependencies = [
    '!' + config.buildDir + '{directives,services}/**/*_test.js',
    config.buildDir + pkg.name + '.module.js',
    config.buildDir + 'directives/**/*.module.js',
    config.buildDir + '{directives,services}/**/*.{js,css}'];

var ignorePath = config.buildDir;
if (ignorePath[0] === '.') {
    // Strip the leading dot ('.') from the ignorePath
    ignorePath = ignorePath.substr(1);
}

/**
 * A task that moves all source into the build staging area where is can be served for testing and subsequently packaged
 * and released. There are multiple types of source and each type has a discrete process that is required to move it from
 * the project source into the build staging area. The types include the following:
 *
 * - js
 * - css
 * - html
 * - fonts
 * - exdep
 *
 * For more information on any specific type see the type specific task in this build. It will be named as: build:<type>.
 * For example, build:js is the task that implements the javascript source building logic.
 */
gulp.task('build', ['build:js', 'build:css', 'build:html', 'build:fonts', 'build:exdep'], function () {
    log({build: 'Injecting sample app: `' + config.sampleIndex + '` --> `' + config.buildDir + config.sampleIndex.substr(2) /*strip `./` from sampleIndex*/ + '`'});

    return gulp.src([config.sampleIndex])
        .pipe($.plumber())
        .pipe($.if(args.verbose, $.debug({title: 'build'})))
        .pipe(wiredep(config.wiredepDefaultOptions))
        .pipe($.inject(gulp.src(localDependencies, {read: false}), {
            ignorePath: ignorePath,
            addRootSlash: false
        }))
        .pipe(gulp.dest(config.buildDir))
        .pipe(browserSync.reload({stream: true}));
});

/**
 * A task to vet all the javascript (*.js) source in the project. This task runs the gulp `jscs` and `jshint` plugins
 * and reports all errors and warnings. If no errors are detected the component source will be copied to the build dir.
 */
gulp.task('build:js', function () {
    // TODO: see about copying just that which has changed.
    log({'build:js': 'Linting (with jshint and jscs) all javascript source code --> ' + config.allJs});
    return gulp.src(config.allJs)
        .pipe($.plumber())
        .pipe($.if(args.verbose, $.debug({title: 'build:js'})))
        .pipe($.if(args.verbose, $.print()))
        .pipe($.jscs())
        .pipe($.jshint())
        .pipe($.jshint.reporter('jshint-stylish', {
            verbose: true
        }))
        .pipe($.jshint.reporter('fail'))
        .pipe(gulp.dest(config.buildDir))
        .pipe(browserSync.reload({stream: true}));
});

/**
 * A task to compile style source (config.componentLess) to CSS. The resulting CSS files are written to the build
 * directory (config.buildDir) into a file structure that parallels the source file structure, but where all the
 * compiled source has a `.css` file extension (the names are otherwise the same).
 */
gulp.task('build:css', function () {
    log({'build:css': 'Compiling LESS to CSS: `' + config.componentLess + '` --> `' + config.buildDir + '**/*.css`'});
    return gulp.src(config.componentLess)
        .pipe($.plumber())
        .pipe($.if(args.verbose, $.debug({title: 'build:css'})))
        .pipe($.less())
        // TODO: consider a broader version range--probably required to work with ie.
        .pipe($.autoprefixer({browsers: ['last 2 version', '> 5%']}))
        .pipe(gulp.dest(config.buildDir))
        .pipe($.csslint())
        .pipe($.csslint.reporter())
        .pipe(browserSync.reload({stream: true}));
});

/**
 * A task to transform all the component template HTML content into AngularJS modules which inject the source of the
 * templates into the `$templateCache` at runtime.
 */
gulp.task('build:html', function () {
    var tmplOutFileName = 'directives/' + pkg.name + '.templates.js';

    log({'build:html': 'Building template cache: `' + config.componentTemplateHtml + '` --> `' + config.buildDir + tmplOutFileName + '`'});

    return gulp.src(config.componentTemplateHtml)
        .pipe($.plumber())
        .pipe($.if(args.verbose, $.debug({title: 'build:html'})))
        .pipe($.html2js({
            outputModuleName: config.rootModuleName,
            useStrict: true
        }))
        .pipe($.concat(tmplOutFileName))
        .pipe(gulp.dest(config.buildDir))
        .pipe(browserSync.reload({stream: true}));
});

/**
 * A task to copy external fonts into the project's build for packaging.
 *
 * @return {Stream}
 */
gulp.task('build:fonts', function () {
    log({'build:fonts': 'Copying fonts:  `' + config.fonts + '` --> `' + config.buildDir + 'fonts/**/*.*`'});

    return gulp
        .src(config.fonts)
        .pipe($.if(args.verbose, $.debug({title: 'build:fonts'})))
        .pipe(gulp.dest(config.buildDir + 'fonts'))
        .pipe(browserSync.reload({stream: true}));
});

/**
 * A task to copy all the installed bower dependencies into the build directory.
 */
gulp.task('build:exdep', function () {
    log({'build:exdep': 'Copying bower dependencies: `' + config.bowerDir + '**` --> `' + config.buildDir + 'bower_components/**`'});

    return $.bower()
        .pipe(gulp.dest(config.buildDir + 'bower_components/'))
        .pipe(browserSync.reload({stream: true}));
});