'use strict';
// jshint node:true

var pkg = require('../../package.json');
var gulp = require('gulp');
var $ = require('gulp-load-plugins')({lazy: true});
var args = require('yargs').argv;
var config = require('../config');
var log = require('../util/log');

/**
 *
 */
gulp.task('release', ['release:tag'], function () {
    log({release: 'build-target --> ' + config.distDir + 'NOT YET IMPLEMENTED'});
});

// SEE: http://ponyfoo.com/articles/my-first-gulp-adventure
// This link shows an implementation of tag and bump.

gulp.task('release:bump', ['package'], function () {
    log({'release:bump': 'build-target --> ' + config.distDir + 'NOT YET IMPLEMENTED'});

    return gulp.src(['../../package.json'/*, './bower.json'*/])//TODO: determine if we want to put a version in bower.json...it's deprecated, but seems standard.
        .pipe($.bump())
        .pipe(gulp.dest('./'));
});

gulp.task('release:tag', ['release:bump'], function () {
    log({'release:tag': 'build-target --> ' + config.distDir + 'NOT YET IMPLEMENTED'});

    var v = 'v' + pkg.version;
    var message = 'Release ' + v;

    return gulp.src('./')
        .pipe($.git.commit(message))
        .pipe($.git.tag(v, message))
        .pipe($.git.push('origin', 'master', '--tags'))
        .pipe(gulp.dest('./'));
});