'use strict';
// jshint node:true

var gulp = require('gulp');
var $ = require('gulp-load-plugins')({lazy: true});
var config = require('../config');
var log = require('../util/log');

gulp.task('help', $.taskListing.withFilters(/:/));
gulp.task('default', ['build', 'watch']);