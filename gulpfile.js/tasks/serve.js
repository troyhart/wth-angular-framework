'use strict';
// jshint node:true

var gulp = require('gulp');
var args = require('yargs').argv;
var browserSync = require('browser-sync').create();
var config = require('../config');
var log = require('../util/log');

/**
 * A task to serve the build in a live reloading server (browser-sync).
 */
gulp.task('serve', function () {
    browserSync.init({
        server: {
            baseDir: config.buildDir
        }
    });
});