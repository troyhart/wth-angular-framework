'use strict';
// jshint node:true

var gulp = require('gulp');
var args = require('yargs').argv;

var config = require('../config');
var log = require('../util/log');
var startTests = require('../util/karmaRunner.js');

/**
 * A task to watch the sources that requires processing or
 * that we want to verify and test.
 */
gulp.task('watch', ['serve'], function (done) {
    log({watch: 'Will execute `build:js` on change in --> ' + config.allJs});
    gulp.watch(config.allJs, ['build:js']);

    log({watch: 'Will execute `build:css` on change in --> ' + config.componentLess});
    gulp.watch(config.componentLess, ['build:css']);

    log({watch: 'Will execute `build:html` on change in --> ' + config.componentTemplateHtml});
    gulp.watch(config.componentTemplateHtml, ['build:html']);

    // TODO: consider watching bower_components to trigger build:exdep

    log({watch: 'starting karma test runner in watch more -- will watch files[] specified in karma.conf.js'});
    // start karma test runner in watch mode -- for watched files, see: karma.conf.js:files[].
    startTests(false /* !singleRun */, done);
});