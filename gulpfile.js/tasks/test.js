'use strict';
// jshint node:true

var gulp = require('gulp');
var args = require('yargs').argv;
var config = require('../config');
var log = require('../util/log');
var startTests = require('../util/karmaRunner.js');

/**
 * A task to run all the tests once and exit.
 */
gulp.task('test', ['build'], function (done) {
    startTests(true /*singleRun*/, done);
});