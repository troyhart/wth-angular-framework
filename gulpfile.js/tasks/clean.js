'use strict';
// jshint node:true

var gulp = require('gulp');
var args = require('yargs').argv;
var clean = require('../util/clean');
var config = require('../config');
var log = require('../util/log');

/**
 * A task to delete all the build and package folders: config.distDir and config.buildDir.
 *
 * @param  {Function} done - callback when complete
 */
gulp.task('clean', function (done) {
    var dirs = [config.distDir, config.buildDir];
    log({clean: 'Deleting -->' + dirs});
    clean(dirs, done);
});

/**
 * A task to delete the package folder: config.distDir.
 */
gulp.task('clean:package', function (done) {
    var dirs = [config.distDir];
    log({'package:clean': 'Deleting -->' + dirs});
    clean(dirs, done);
});