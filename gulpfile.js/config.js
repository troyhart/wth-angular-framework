'use strict';
// jshint node:true

var componentsBaseDir = './src/';
var sampleAppRootDir = './';
var bowerDir = './bower_components/';
var buildDir = './build/';
var distDir = './dist/';
var rootModuleName = 'wthAngularFramework';// this module is defined in src/directives/wth-angular-framework.module.js
var bower = {
    json: require('../bower.json'),
    directory: bowerDir
    //ignorePath: '../..'
};

var config = {
    rootModuleName: rootModuleName,
    sampleAppRootDir: sampleAppRootDir,
    compenentsBaseDir: componentsBaseDir,
    bowerDir: bowerDir,
    buildDir: buildDir,
    distDir: distDir,
    sampleIndex: sampleAppRootDir + 'index.html',
    fonts: [bowerDir + 'font-awesome/fonts/**/*.*'],
    // all the js source that will be vetted.
    allJs: ['./src/**/*.js', './*.js'],
    // all the less source
    componentLess: ['./src/**/*.less'],
    componentTemplateHtml: [componentsBaseDir + '**/*.template.html'],
    componentJs: [
        componentsBaseDir + '**/*.module.js',
        componentsBaseDir + '**/*.js',
        '!' + componentsBaseDir + '**/*_test.js'
    ],
    bower: bower,
    wiredepDefaultOptions: {
        bowerJson: bower.json,
        directory: bower.directory,
        ignorePath: bower.ignorePath
    }
};

module.exports = config;