'use strict';
// jshint node:true

var server = require('karma').server;

var args = require('yargs').argv;
var config = require('../config');
var log = require('../util/log');

var child;
var excludeFiles = [];

/**
 * Start the tests using karma.
 *
 * @param  {boolean} singleRun - True means run once and end (CI), or keep running (dev)
 * @param  {Function} done - Callback to fire when karma is done
 *
 * @return {undefined}
 */
function startTests(singleRun, done) {

    server.start({
        configFile: __dirname + '/../../karma.conf.js',
        exclude: excludeFiles,
        singleRun: !!singleRun
    }, karmaCompleted);

    ////////////////

    function karmaCompleted(karmaResult) {
        log({message: 'Karma completing'});
        if (child) {
            log({karma: 'Shutting down the child process'});
            child.kill();
        }
        if (karmaResult === 1) {
            done({karma: 'Tests failed with code ' + karmaResult});
        } else {
            done();
        }
    }
}

module.exports = startTests;