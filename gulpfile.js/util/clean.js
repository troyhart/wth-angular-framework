'use strict';
// jshint node: true

var del = require('del');

var args = require('yargs').argv;
var config = require('../config');
var log = require('../util/log');

/**
 * A function that helps keep the filesystem clean by deleting all the given pathSelectors.
 *
 * @param {string[]} pathsSelectors - an array of paths.
 * @param {Function} done - a callback to be executed when the paths have all been deleted.
 */
function clean(pathsSelectors, done) {
    del(pathsSelectors).then(function (paths) {
        if (args.verbose) {
            log({cleaned: paths});
        }
        // execute the callback because we are done!
        done();
    });
}

module.exports = clean;