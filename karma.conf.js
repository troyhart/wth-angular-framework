'use strict';

module.exports = function (config) {
    var basePath = './build/';
    config.set({

        basePath: basePath,

        files: [
            'bower_components/angular/angular.js',
            'bower_components/angular-mocks/angular-mocks.js',
            'directives/**/*.module.js',
            'directives/**/*.js'
        ],

        reporters: ['progress'],

        autoWatch: true,

        frameworks: ['jasmine'],

        browsers: ['Chrome'],

        plugins: [
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-jasmine',
            'karma-junit-reporter'
        ],

        junitReporter: {
            outputDir: basePath + 'karma-out/',
            outputFile: undefined,
            suite: ''
        }

    });
};
